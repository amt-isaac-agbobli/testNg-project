import org.testng.annotations.*;

public class NewTest {
    @Test(dataProvider = "data-provider")
    public void test( String data) {
        System.out.println("Data is: " + data);
        System.out.println("Test");
    }
    @BeforeClass
    void setUpConfig() {
        System.out.println("Setting up configuration");
    }

    @AfterClass
    void tearDownConfig() {
        System.out.println("Tearing down configuration");
    }

    @BeforeMethod
    void setUpData() {
        System.out.println("Setting up data");
    }

    @AfterMethod
    void tearDownData() {
        System.out.println("Tearing down data");
    }

    @DataProvider(name = "data-provider")
    public Object[][] dpMethod(){
        return new Object[][] {{"data1"}, {"data2"}};
    }

}
