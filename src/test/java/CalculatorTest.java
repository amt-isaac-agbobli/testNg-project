import com.isaac.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CalculatorTest {
     Calculator calculator ;

    @BeforeClass
    void setUp() {
     calculator = new Calculator();
    }

    @Test(description = "Test add method", groups = {"smoke"})
    public void testAdd() {
        assert calculator.add(1, 1) == 2;
    }

    @Test(description = "Test subtract method", groups = {"smoke"})
    public void testSubtract() {
        assert calculator.subtract(2, 1) == 1;
    }

    @DataProvider(name = "data-provider")
    public Object[][] dpMethod(){
        return new Object[][] {{1, 3, 4}, {2, 7, 9}};
    }

    @Test(dataProvider = "data-provider", description = "Test add method with data provider", groups = {"data-driven"})
    public void testAdd(int a, int b, int expected) {
        int result = calculator.add(a, b);
        System.out.println("Result: " + result);
        Assert.assertEquals(result, expected);
    }
}
